# OpenML dataset: Motorcycle-Dataset

https://www.openml.org/d/43801

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains information about used motorcycles
This data can be used for a lot of purposes such as price prediction to exemplify the use of linear regression in Machine Learning.
The columns in the given dataset are as follows:

name
selling price
year
seller type
owner
km driven
ex showroom price

For used car datasets please go to  https://www.kaggle.com/nehalbirla/vehicle-dataset-from-cardekho

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43801) of an [OpenML dataset](https://www.openml.org/d/43801). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43801/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43801/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43801/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

